#![allow(clippy::eq_op)]
#![allow(clippy::identity_op)]

mod arithmatic;
mod control;
mod instructions;

use std::ops::{Deref, DerefMut};

use self::instructions::Instruction;
use crate::{define_bitflag, memory::Memory};

#[derive(Debug)]
pub struct Status(u8);

impl Status {
	define_bitflag!(negative, 7);
	define_bitflag!(overflow, 6);
	define_bitflag!(unused, 5);
	define_bitflag!(r#break, 4);
	define_bitflag!(decimal, 3);
	define_bitflag!(irq_disable, 2);
	define_bitflag!(zero, 1);
	define_bitflag!(carry, 0);
}

impl Default for Status {
	fn default() -> Self {
		Self(
			0 << 7 | // Negative
            0 << 6 | // Overflow
            1 << 5 | // Always 1
            1 << 4 | // Break
            0 << 3 | // Decimal
            1 << 2 | // Irq
            0 << 1 | // Zero
            0 << 0, // Carry
		)
	}
}

impl Deref for Status {
	type Target = u8;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Status {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

#[derive(Debug)]
pub struct Registers {
	/// Accumulator
	pub a: u8,
	/// Index X
	pub x: u8,
	/// Index Y
	pub y: u8,
	/// Program Counter
	pub pc: u16,
	/// Stack Pointer
	pub s: u8,
	/// Status
	pub p: Status,
	/// Internal Per-Instruction work register for storing values between cycles
	pub work_register: u16,
}

impl Default for Registers {
	fn default() -> Self {
		Self { a: 0, x: 0, y: 0, pc: 0, s: 0xFD, p: Status::default(), work_register: 0 }
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Cycle {
	Read,
	Write,
}

#[derive(Debug)]
pub struct Cpu {
	pub registers: Registers,
	/// Cycle of current instruction
	pub cycle: u8,
	pub current_instruction: Option<&'static dyn Instruction>,
	pub previous_cycle: Option<Cycle>,
	pub prefetch: Option<u8>,
}

impl Cpu {
	fn fetch(&mut self, memory: &mut Memory) -> u8 {
		let res = memory.cpu_read(self.registers.pc);
		self.registers.pc += 1;
		res
	}

	fn handle_ins(&mut self, memory: &mut Memory, instruction: &dyn Instruction) {
		let res = instruction.handle(self, memory, self.cycle);
		self.previous_cycle = Some(instruction.cycles()[self.cycle as usize - 1]);
		self.cycle += 1;

		if let Some(prefetch) = res {
			self.cycle = 1;
			self.current_instruction = None;
			self.registers.work_register = 0; // Clear internal per-instruction work register

			self.prefetch = match prefetch {
				true => {
					let res = Some(self.fetch(memory));
					self.cycle += 1;
					res
				}
				false => None,
			};
		} else if self.cycle as usize > instruction.cycles().len() {
			panic!(
				"At cycle {} of op {} which is invalid as it should only be at most {} cycles",
				self.cycle,
				instruction.opcode(),
				instruction.cycles().len()
			)
		} else {
			self.prefetch = None;
		}
	}

	pub fn cycle(&mut self, memory: &mut Memory) {
		if self.current_instruction.is_none() && self.cycle > 1 {
			log::debug!("CPU Cycle {} of {:?} after prefetch", self.cycle, self.prefetch);
		} else {
			log::debug!("CPU Cycle {} of {:?}", self.cycle, self.current_instruction);
		}

		if let Some(instruction) = self.current_instruction {
			self.handle_ins(memory, instruction)
		} else {
			let opcode = match self.prefetch {
				Some(prefetch) => prefetch,
				None => {
					let res = self.fetch(memory);
					self.cycle += 1;
					self.previous_cycle = Some(Cycle::Read);
					res
				}
			};

			self.current_instruction = Some(match instructions::INSTRUCTIONS.get(&opcode) {
				Some(instruction) => instruction.deref(),
				None => unimplemented!("Opcode {:#X}", opcode),
			});

			if self.prefetch.is_some() {
				self.prefetch = None;
				self.handle_ins(memory, self.current_instruction.unwrap());
			}
		}
	}

	pub fn reset(&mut self, memory: &mut Memory) {
		self.registers.s -= 3;
		self.registers.p.set_irq_disable(true);
		self.registers.pc = memory.reset_vector();
	}
}

impl Default for Cpu {
	fn default() -> Self {
		Self {
			registers: Default::default(),
			cycle: 1,
			current_instruction: Default::default(),
			previous_cycle: Default::default(),
			prefetch: Default::default(),
		}
	}
}
