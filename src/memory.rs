use crate::cartridge::Cartridge;

pub enum NametableMirroring {
	Horizontal,
	Vertical,
	SingleScreen,
	FourScreen,
}

pub struct Nametable {
	pub internal: [u8; 0x400],
}

pub struct Memory {
	pub nametable_one: [u8; 0x400],
	pub nametable_two: [u8; 0x400],
	pub mirroring_mode: NametableMirroring,
	pub iram: [u8; 0x800],
	pub cartridge: Option<Box<dyn Cartridge>>,
}

impl Default for Memory {
	fn default() -> Self {
		let mut out = Self {
			iram: [0; 0x800],
			cartridge: None,
			nametable_one: [0; 0x400],
			nametable_two: [0; 0x400],
			mirroring_mode: NametableMirroring::Horizontal,
		};

		for x in 0..(0x800 / 4) {
			const MEW: &[u8; 4] = b"Mew!";
			#[allow(clippy::needless_range_loop)]
			for y in 0..4 {
				out.iram[(x * 4) + y] = MEW[y];
			}
		}

		out
	}
}

impl Memory {
	pub fn cpu_read(&mut self, index: u16) -> u8 {
		match index {
			0..=0x1FFF => self.iram[index as usize % 0x800],
			0x4020..=0xFFFF => {
				self.cartridge.as_deref_mut().map_or(0, |c| c.mapper().cpu_read(index))
			}
			_ => unimplemented!("Unimplemented CPU Read {:#X}", index),
		}
	}

	pub fn cpu_write(&mut self, index: u16, value: u8) {
		match index {
			0..=0x1FFF => self.iram[index as usize % 0x800] = value,
			0x4020..=0xFFFF => {
				if let Some(cart) = self.cartridge.as_deref_mut() {
					cart.mapper().cpu_write(index, value);
				}
			}
			_ => unimplemented!("Unimplemented CPU write {:#X} @ {:#X}", value, index),
		}
	}

	pub fn nmi_vector(&mut self) -> u16 {
		self.cartridge.as_deref_mut().map_or(0, |c| {
			((c.mapper().cpu_read(0xFFFB) as u16) << 8) | c.mapper().cpu_read(0xFFFA) as u16
		})
	}

	pub fn reset_vector(&mut self) -> u16 {
		self.cartridge.as_deref_mut().map_or(0, |c| {
			((c.mapper().cpu_read(0xFFFD) as u16) << 8) | c.mapper().cpu_read(0xFFFC) as u16
		})
	}

	pub fn irq_vector(&mut self) -> u16 {
		self.cartridge.as_deref_mut().map_or(0, |c| {
			((c.mapper().cpu_read(0xFFFF) as u16) << 8) | c.mapper().cpu_read(0xFFFE) as u16
		})
	}
}
