use std::fmt::{Debug, Display};

use super::{arithmatic, control, Cpu, Cycle};
use crate::memory::Memory;

#[macro_export(crate)]
macro_rules! bad_cycle {
	($i:ident, $c:ident) => {
		panic!("Cycle {} should not be reachable for {}", $c, $i)
	};
}

#[macro_export(crate)]
macro_rules! prefetch_fail {
	() => {
		Some(false)
	};
}

#[macro_export(crate)]
macro_rules! prefetch_success {
	() => {
		Some(true)
	};
}

#[macro_export(crate)]
macro_rules! more_cycles {
	() => {
		None
	};
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Operand {
	/// None
	None,
	/// 8 bit immediate
	Immediate,
	/// 0xXX
	ZeroPaged,
	/// ZeroPaged + X (OVERFLOWS AS 8BIT)
	ZeroPagedX,
	/// ZeroPaged + Y (OVERFLOWS AS 8BIT)
	ZeroPagedY,
	/// *ZeroPaged + X
	DerefZeroPagedX,
	/// *(ZeroPaged + Y)
	DerefZeroPagedY,
	/// 0xXXXX
	Absolute,
	/// 0xXXXX + Y
	AbsoluteX,
	/// 0xXXXX + X
	AbsoluteY,
	/// *0xXXXX
	DerefAbsolute,
	/// PC + 0xXX
	PcRelative,
}

pub trait Instruction: Debug + Display + Send + Sync {
	fn opcode(&self) -> u8;
	fn cycles(&self) -> &'static [Cycle];
	fn handle(&self, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool>;
}

macro_rules! define_instruction {
	($opcode:literal, $display:literal, $operand:expr, $cycles:expr, $handler:path) => {
		paste::paste! {
			pub struct [<Instruction $opcode>];

			impl Instruction for [<Instruction $opcode>] {
				fn opcode(&self) -> u8 {
					$opcode
				}

				fn cycles(&self) -> &'static [Cycle] {
					&$cycles
				}

				fn handle(&self, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
					$handler(self, cpu, memory, cycle)
				}
			}

			impl std::fmt::Display for [<Instruction $opcode>] {
				fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
					f.write_fmt(format_args!("{} ({:?})", $display, $operand))
				}
			}

			impl std::fmt::Debug for [<Instruction $opcode>] {
				fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
					f.write_fmt(format_args!("{} ({:?})", $display, $operand))
				}
			}
		}
	};
}

macro_rules! define_instructions {
    ($( $opcode:literal, $display:literal, $operand:expr, $cycles:expr, $handler:path );*) => {
        $( define_instruction! { $opcode, $display, $operand, $cycles, $handler } )*

        lazy_static::lazy_static! {
            pub static ref INSTRUCTIONS: std::collections::HashMap<u8, Box<dyn Instruction>> = {
                let mut ins = std::collections::HashMap::new();
                paste::paste! {
                    $( ins.insert($opcode,  Box::new([<Instruction $opcode>]) as Box<dyn Instruction>); )*
                }
                ins
            };
        }
    };
}

define_instructions! {
	0x10, "BPL", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bpl;
	0x18, "CLC", Operand::None, [Cycle::Read, Cycle::Read], control::clc;
	0x30, "BMI", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bmi;
	0x38, "SEC", Operand::None, [Cycle::Read, Cycle::Read], control::sec;
	0x4A, "LSR", Operand::None, [Cycle::Read, Cycle::Read], arithmatic::lsr_imp;
	0x4C, "JMP a", Operand::Absolute, [Cycle::Read, Cycle::Read, Cycle::Read], control::jmp_abs;
	0x50, "BVC", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bvc;
	0x58, "CLI", Operand::None, [Cycle::Read, Cycle::Read], control::cli;
	0x70, "BVS", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bvs;
	0x78, "SEI", Operand::None, [Cycle::Read, Cycle::Read], control::sei;
	0x8D, "STA a", Operand::Absolute, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Write], arithmatic::sta_abs;
	0x90, "BCC", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bcc;
	0x9A, "TXS", Operand::None, [Cycle::Read, Cycle::Read], arithmatic::txs;
	0xA2, "LDX #i", Operand::Immediate, [Cycle::Read, Cycle::Read], arithmatic::ldx_imm;
	0xA9, "LDA #i", Operand::Immediate, [Cycle::Read, Cycle::Read], arithmatic::lda_imm;
	0xB0, "BCS", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bcs;
	0xB8, "CLV", Operand::None, [Cycle::Read, Cycle::Read], control::clv;
	0xC9, "CMP #i", Operand::Immediate, [Cycle::Read, Cycle::Read], arithmatic::cmp_imm;
	0xD0, "BNE", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::bne;
	0xD8, "CLD", Operand::None, [Cycle::Read, Cycle::Read], control::cld;
	0xEA, "NOP", Operand::None, [Cycle::Read, Cycle::Read], control::nop_imp;
	0xF0, "BEQ", Operand::None, [Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read, Cycle::Read], control::beq;
	0xF8, "SED", Operand::None, [Cycle::Read, Cycle::Read], control::sed
}
