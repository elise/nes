use super::{instructions::Instruction, Cpu};
use crate::{bad_cycle, memory::Memory, more_cycles, prefetch_fail, prefetch_success};

fn conditional_relative_branch_internal(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
	conditional: bool,
) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.work_register = cpu.fetch(memory) as u16;
			more_cycles!()
		}
		3 => match conditional {
			false => prefetch_success!(),
			true => {
				let offset = cpu.registers.work_register as i8;
				cpu.registers.work_register = match offset.is_negative() {
					false => cpu.registers.pc + (cpu.registers.work_register as i8).abs() as u16,
					true => cpu.registers.pc - (cpu.registers.work_register as i8).abs() as u16,
				};
				cpu.registers.pc =
					(cpu.registers.pc & 0xFF00) | (cpu.registers.work_register & 0xFF);
				more_cycles!()
			}
		},
		4 => match cpu.registers.pc == cpu.registers.work_register {
			false => {
				cpu.registers.pc = cpu.registers.work_register;
				more_cycles!()
			}
			true => prefetch_success!(),
		},
		5 => prefetch_success!(),
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn bpl(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, !cpu.registers.p.negative())
}

pub fn bmi(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, cpu.registers.p.negative())
}

pub fn bvc(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, !cpu.registers.p.overflow())
}

pub fn bvs(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, cpu.registers.p.overflow())
}

pub fn bcc(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, !cpu.registers.p.carry())
}

pub fn bcs(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, cpu.registers.p.carry())
}

pub fn bne(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, !cpu.registers.p.zero())
}

pub fn beq(instr: &dyn Instruction, cpu: &mut Cpu, memory: &mut Memory, cycle: u8) -> Option<bool> {
	conditional_relative_branch_internal(instr, cpu, memory, cycle, cpu.registers.p.zero())
}

pub fn jmp_abs(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.work_register = cpu.fetch(memory) as u16;

			more_cycles!()
		}
		3 => {
			let higher = cpu.fetch(memory);
			cpu.registers.pc = (higher as u16) << 8 | cpu.registers.work_register;

			prefetch_fail!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn clc(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_carry(false);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn sec(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_carry(true);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn cld(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_decimal(false);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn sed(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_decimal(true);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn cli(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_irq_disable(false);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn sei(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_irq_disable(true);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn clv(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.p.set_overflow(false);

			prefetch_success!()
		}
		cycle => bad_cycle!(instr, cycle),
	}
}

pub fn nop_imp(instr: &dyn Instruction, _: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => prefetch_success!(),
		cycle => bad_cycle!(instr, cycle),
	}
}
