use super::{Cpu, Instruction};
use crate::{memory::Memory, more_cycles, prefetch_fail, prefetch_success};

pub fn lda_imm(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
) -> Option<bool> {
	match cycle {
		2 => {
			let read_val = cpu.fetch(memory);

			cpu.registers.a = read_val;
			cpu.registers.p.set_zero(read_val == 0);
			cpu.registers.p.set_negative((read_val as i8).is_negative());

			prefetch_fail!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}

pub fn ldx_imm(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
) -> Option<bool> {
	match cycle {
		2 => {
			let read_val = cpu.fetch(memory);

			cpu.registers.x = read_val;
			cpu.registers.p.set_zero(read_val == 0);
			cpu.registers.p.set_negative((read_val as i8).is_negative());

			prefetch_fail!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}

pub fn sta_abs(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.work_register |= cpu.fetch(memory) as u16;
			more_cycles!()
		}
		3 => {
			cpu.registers.work_register |= (cpu.fetch(memory) as u16) << 8;
			more_cycles!()
		}
		4 => {
			memory.cpu_write(cpu.registers.work_register, cpu.registers.a);
			prefetch_fail!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}

pub fn lsr_imp(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			let carry = cpu.registers.a & 0b1 == 1;
			cpu.registers.a >>= 1;

			cpu.registers.p.set_negative((cpu.registers.a as i8).is_negative());
			cpu.registers.p.set_zero(cpu.registers.a == 0);
			cpu.registers.p.set_carry(carry);

			prefetch_success!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}

fn cmp_internal(cpu: &mut Cpu, argument: u8) {
	let res = cpu.registers.a - argument;
	cpu.registers.p.set_negative((res as i8).is_negative());
	cpu.registers.p.set_zero(res == 0);
	cpu.registers.p.set_carry(true);
}

pub fn cmp_imm(
	instr: &dyn Instruction,
	cpu: &mut Cpu,
	memory: &mut Memory,
	cycle: u8,
) -> Option<bool> {
	match cycle {
		2 => {
			let imm = cpu.fetch(memory);

			cmp_internal(cpu, imm);

			prefetch_fail!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}

/// Not actually an arithmatic operation, just going here as other `tXX` ops are
pub fn txs(instr: &dyn Instruction, cpu: &mut Cpu, _: &mut Memory, cycle: u8) -> Option<bool> {
	match cycle {
		2 => {
			cpu.registers.s = cpu.registers.x;

			prefetch_success!()
		}
		cycle => crate::bad_cycle!(instr, cycle),
	}
}
