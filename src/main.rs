#![allow(dead_code)]

mod cartridge;
mod memory;
mod nes;
mod r602;

use std::{io::Write, path::PathBuf};

use argh::FromArgs;
use cartridge::CartridgeLoadError;
use nes::Nes;
use thiserror::Error;

#[derive(Debug, FromArgs)]
/// NES Emulator
struct Args {
	#[argh(positional)]
	/// ROM Path
	pub rom_path: PathBuf,
}

#[derive(Debug, Error)]
pub enum NesError {
	#[error("Cartridge Error: `{0}`")]
	CatridgeLoad(#[from] CartridgeLoadError),
	#[error("IO Error: `{0}`")]
	IO(#[from] std::io::Error),
}

fn main() -> Result<(), NesError> {
	env_logger::init();

	let args: Args = argh::from_env();

	let cart = std::fs::read(args.rom_path.as_path())?;

	let mut nes = Nes::default();
	nes.load_cartridge(cart)?;

	let mut c = false;

	loop {
		match c {
			true => nes.cycle(),
			false => {
				let mut buffer = String::new();
				print!("> ");
				std::io::stdout().flush()?;
				std::io::stdin().read_line(&mut buffer)?;

				match buffer.chars().next().unwrap_or('\n').to_ascii_lowercase() {
					'c' => c = true,
					'd' => std::fs::write("dump.bin", nes.dump_memory())?,
					'e' => break,
					'r' => println!("{}", nes.registers()),
					's' => nes.cycle(),
					'\n' => {}
					_ => print!(
						"Help
c - continue
d - dumps memory to dump.bin
e - exits
r - dump registers to stdout
s - Step
"
					),
				}
			}
		}
	}

	Ok(())
}
