mod mmc1;

pub(crate) use mmc1::Mmc1;

pub trait Mapper {
	fn new(prg_rom: Vec<u8>, chr_rom: Vec<u8>, prg_ram: Option<Vec<u8>>) -> Self
	where
		Self: Sized;
	fn cpu_read(&self, index: u16) -> u8;
	fn cpu_write(&mut self, index: u16, value: u8);
	fn ppu_read(&self, index: u16) -> u8;
}
