use super::{mapper::Mapper, Cartridge};

pub struct Nes2;

impl Cartridge for Nes2 {
	fn load(
		_: &mut crate::memory::Memory,
		_: Vec<u8>,
	) -> Result<Box<dyn Cartridge>, super::CartridgeLoadError> {
		unimplemented!("NES2 unsupported");
	}

	fn mapper(&mut self) -> &mut dyn Mapper {
		todo!()
	}
}
