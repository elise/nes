use super::Mapper;
use crate::define_bitflag;

pub struct Mmc1 {
	prg_rom: Vec<u8>,
	chr_rom: Vec<u8>,
	prg_ram: Option<Vec<u8>>,
	internal_shift_register: u8,
	internal_ctr: u8,
	internal_control: u8,
	internal_chr_0: u8,
	internal_chr_1: u8,
	internal_prg: u8,
}

impl Mmc1 {
	define_bitflag!(internal_control, chr_rom_mode, 4);
	define_bitflag!(internal_control, prg_bank_size, 3);
	define_bitflag!(internal_control, prg_swap_bank_toggle, 2);

	pub fn mirroring_mode(&self) -> u8 {
		self.internal_control & 0b11
	}

	pub fn prg_rom_bank_mode(&self) -> u8 {
		(self.internal_control >> 2) & 0b11
	}

	pub fn prg_bank(&self) -> u8 {
		self.internal_prg & 0b1111
	}

	pub fn prg_bank_trim_final(&self) -> u8 {
		self.internal_prg & 0b1110
	}

	pub fn prg_rom_bank_one(&self) -> &[u8] {
		let idx = match self.prg_bank_size() {
			// 32KB
			false => (self.prg_bank_trim_final() as usize * (1024 * 32)),
			// 16KB
			true => match self.prg_swap_bank_toggle() {
				false => 0,
				true => (self.prg_bank() as usize) * (1024 * 16),
			},
		};
		&self.prg_rom[idx..idx + 0x4000]
	}

	pub fn prg_rom_bank_two(&self) -> &[u8] {
		let idx = match self.prg_bank_size() {
			// 32KB
			false => (self.prg_bank_trim_final() as usize * (1024 * 32)) + (1024 * 16),
			// 16KB
			true => match self.prg_swap_bank_toggle() {
				false => (self.prg_bank() as usize) * (1024 * 16),
				true => self.prg_rom.len() - (1024 * 16),
			},
		};
		&self.prg_rom[idx..idx + 0x4000]
	}

	pub fn chr_bank_one(&self) -> &[u8] {
		let bank = self.internal_chr_0
			& match self.chr_rom_mode() {
				true => 0b0001_1111,
				false => 0b0001_1110,
			};

		let idx = bank as usize * (1024 * 8);

		&self.chr_rom[idx..(idx + (1024 * 4))]
	}

	pub fn chr_bank_two(&self) -> &[u8] {
		let idx = match self.chr_rom_mode() {
			true => self.internal_chr_1 as usize * (1024 * 4),
			false => (self.internal_chr_0 & 0b0001_1110) as usize * (1024 * 8) + (1024 * 4),
		};

		&self.chr_rom[idx..(idx + (1024 * 4))]
	}
}

impl Mapper for Mmc1 {
	fn new(prg_rom: Vec<u8>, chr_rom: Vec<u8>, prg_ram: Option<Vec<u8>>) -> Self
	where
		Self: Sized,
	{
		Self {
			prg_rom,
			chr_rom,
			prg_ram,
			internal_shift_register: 0,
			internal_ctr: 0,
			internal_control: 0b01100,
			internal_chr_0: 0,
			internal_chr_1: 0,
			internal_prg: 0,
		}
	}

	fn cpu_read(&self, index: u16) -> u8 {
		log::debug!("Read @ {:#X}", index);
		let index = index as usize;

		match index {
			0x6000..=0x7FFF => match &self.prg_ram {
				Some(buf) => buf[index % 0x6000],
				None => 0,
			},
			0x8000..=0xBFFF => self.prg_rom_bank_one()[index % 0x8000],
			0xC000..=0xFFFF => self.prg_rom_bank_two()[index % 0xC000],
			_ => 0,
		}
	}

	fn cpu_write(&mut self, index: u16, value: u8) {
		let index = index as usize;
		match index {
			0x6000..=0x7FFF => {
				if let Some(buf) = self.prg_ram.as_deref_mut() {
					buf[index - 0x6000] = value;
				}
			}
			0x8000..=0xFFFF => {
				if value & (1 << 7) != 0 {
					self.internal_shift_register = 0;
					self.internal_ctr = 0;
				} else {
					self.internal_shift_register |= (value & 0b1) << self.internal_ctr;

					self.internal_ctr += 1;

					if self.internal_ctr == 5 {
						match (index >> 13) & 0b11 {
							// Control
							0 => self.internal_control = self.internal_shift_register,
							// CHR 0
							1 => self.internal_chr_0 = self.internal_shift_register,
							// CHR 1
							2 => self.internal_chr_1 = self.internal_shift_register,
							// PRG
							3 => self.internal_prg = self.internal_shift_register,
							_ => unreachable!("int masked to 0b11 can only be 0-3"),
						}
						self.internal_shift_register = 0;
						self.internal_ctr = 0;
					}
				}
			}
			_ => {}
		}
	}

	fn ppu_read(&self, index: u16) -> u8 {
		let index = index as usize;
		match index {
			0..=0xFFF => self.prg_rom_bank_one()[index],
			0x1000..=0x1FFF => self.prg_rom_bank_two()[index % 0x1000],
			_ => 0,
		}
	}
}
