use super::{
	mapper::{self, Mapper},
	Cartridge, CartridgeLoadError,
};
use crate::define_bitflag;

#[derive(Debug, Default)]
pub struct Flag6(pub(crate) u8);

impl Flag6 {
	pub fn lower_mapper_nybble(&self) -> u8 {
		self.0 >> 4
	}

	pub fn set_lower_mapper_nybble(&mut self, value: u8) {
		self.0 &= 0b1111;
		self.0 |= value << 4;
	}

	define_bitflag!(disable_mirroring, 3);
	define_bitflag!(trainer, 2);
	define_bitflag!(prg_ram, 1);
	define_bitflag!(mirroring_arrangement, 0);
}

#[derive(Debug, Default)]
pub struct Flag7(pub(crate) u8);

impl Flag7 {
	pub fn upper_mapper_nybble(&self) -> u8 {
		self.0 >> 4
	}

	pub fn set_upper_mapper_nybble(&mut self, value: u8) {
		self.0 &= 0b1111;
		self.0 |= value & (0b1111 << 4);
	}

	define_bitflag!(playchoice, 1);
	define_bitflag!(vs_unisystem, 0);
}

#[derive(Debug, Default)]
pub struct Flag9(pub(crate) u8);

impl Flag9 {
	define_bitflag!(pal, 0);
}

#[derive(Debug, Default)]
#[repr(C)]
pub struct Header {
	pub magic: [u8; 4],
	/// 16KB units
	pub prg_rom_units: u8,
	/// 8KB units
	pub chr_rom_units: u8,
	pub flag6: Flag6,
	pub flag7: Flag7,
	pub prg_ram: u8,
	pub flag9: Flag9,
	pub padding: [u8; 5],
}

pub struct Nes {
	header: Header,
	mapper: Box<dyn Mapper>,
}

impl Cartridge for Nes {
	#[allow(unused_assignments)]
	fn load(
		memory: &mut crate::memory::Memory,
		input: Vec<u8>,
	) -> Result<Box<dyn Cartridge>, CartridgeLoadError> {
		if input.len() < std::mem::size_of::<Header>() && &input[0..4] != b"NES\x1A" {
			return Err(CartridgeLoadError::InvalidHeader);
		}

		let mut offset = 0;
		let mut header = Header::default();

		header.magic[offset..4].clone_from_slice(&input[offset..4]);
		offset += 4;
		header.prg_rom_units = input[offset];
		offset += 1;
		header.chr_rom_units = input[offset];
		offset += 1;
		header.flag6.0 = input[offset];
		offset += 1;
		header.flag7.0 = input[offset];
		offset += 1;
		header.prg_ram = input[offset];
		offset += 1;
		header.flag9.0 = input[offset];
		offset += 1;

		offset = 16; // End of header is 16

		if header.flag6.trainer() {
			for x in 0x7000..(0x7000 + 512) {
				memory.cpu_write(x, input[offset]);
				offset += 1;
			}
		}

		let prg_rom_len = header.prg_rom_units as usize * (16 * 1024);
		let chr_rom_len = header.chr_rom_units as usize * (8 * 1024);

		let prg_rom = input[offset..(offset + prg_rom_len)].to_owned();
		offset += prg_rom_len;
		let chr_rom = input[offset..(offset + chr_rom_len)].to_owned();
		offset += chr_rom_len;

		let prg_ram = match header.flag6.prg_ram() {
			true => Some(Vec::with_capacity(0x2000)),
			false => None,
		};

		let mapper_id =
			(header.flag7.upper_mapper_nybble() << 4) | header.flag6.lower_mapper_nybble();

		log::info!(
			"Loading ROM with mapper {}. PRG_ROM_16Ks: {:#X} ({:#X}), CHR_ROM_8ks: {:#X} ({:#X})",
			mapper_id,
			prg_rom_len / (16 * 1024),
			prg_rom.len() / (16 * 1024),
			chr_rom_len / (8 * 1024),
			chr_rom.len() / (8 * 1024)
		);
		let mapper: Box<dyn Mapper> = match mapper_id {
			1 => Box::new(mapper::Mmc1::new(prg_rom, chr_rom, prg_ram)),
			mapper_id => return Err(CartridgeLoadError::UnimplementedMapper(mapper_id as u16)),
		};

		Ok(Box::new(Self { header, mapper }))
	}

	fn mapper(&mut self) -> &mut dyn Mapper {
		self.mapper.as_mut()
	}
}
