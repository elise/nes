use std::convert::TryInto;

use crate::{
	cartridge::{self, Cartridge, CartridgeLoadError, CartridgeType},
	memory::Memory,
	r602,
};

#[macro_export(crate)]
macro_rules! define_bitflag {
	($internal_name:ident, $name:ident, $offset:literal) => {
		pub fn $name(&self) -> bool {
			(self.$internal_name & (1 << $offset)) != 0
		}

		paste::paste! {
			pub fn [<set_ $name>](&mut self, value: bool) {
				if value {
					self.$internal_name |= (1 << $offset);
				} else {
					self.$internal_name &= !(1 << $offset);
				}
			}
		}
	};
	($name:ident, $offset:literal) => {
		pub fn $name(&self) -> bool {
			(self.0 & (1 << $offset)) != 0
		}

		paste::paste! {
			pub fn [<set_ $name>](&mut self, value: bool) {
				if value {
					self.0 |= (1 << $offset);
				} else {
					self.0 &= !(1 << $offset);
				}
			}
		}
	};
}

#[derive(Default)]
pub struct Nes {
	cpu: r602::Cpu,
	memory: Memory,
}

impl Nes {
	pub fn load_cartridge(&mut self, input: Vec<u8>) -> Result<(), CartridgeLoadError> {
		if input.len() < 8 {
			return Err(CartridgeLoadError::UnrecognizedFormat);
		}

		let input_type = match CartridgeType::parse_type(&input[0..8].try_into().unwrap()) {
			Some(cartridge_type) => Ok(cartridge_type),
			None => Err(CartridgeLoadError::UnrecognizedFormat),
		}?;

		self.memory.cartridge = Some(match input_type {
			CartridgeType::Nes => cartridge::nes::Nes::load(&mut self.memory, input),
			CartridgeType::Nes2 => cartridge::nes2::Nes2::load(&mut self.memory, input),
		}?);

		self.cpu.registers.pc = self.memory.reset_vector();

		Ok(())
	}

	pub fn dump_memory(&mut self) -> Vec<u8> {
		let start_of_dump = chrono::Local::now();
		let mut memory = vec![0; 0xFFFF + 1];
		#[allow(clippy::needless_range_loop)]
		for x in 0..=0xFFFF {
			memory[x] = self.memory.cpu_read(x as u16);
		}
		let end_of_dump = chrono::Local::now();
		log::info!(
			"Dumped memory in {} µs",
			(end_of_dump - start_of_dump).num_microseconds().unwrap_or_default()
		);
		memory
	}

	pub fn registers(&self) -> String {
		format!("{:#?}", self.cpu.registers)
	}

	pub fn cycle(&mut self) {
		self.cpu.cycle(&mut self.memory);
	}
}
