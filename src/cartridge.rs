use thiserror::Error;

use self::mapper::Mapper;
use crate::memory::Memory;

pub mod mapper;
pub mod nes;
pub mod nes2;

#[derive(Debug, Error)]
pub enum CartridgeLoadError {
	#[error("Unrecognized File Format")]
	UnrecognizedFormat,
	#[error("Invalid File Header")]
	InvalidHeader,
	#[error("Unimplemented Mapper: `{0}`")]
	UnimplementedMapper(u16),
}

pub enum CartridgeType {
	Nes,
	Nes2,
}

impl CartridgeType {
	pub fn parse_type(input: &[u8; 8]) -> Option<CartridgeType> {
		match &input[0..4] {
			b"NES\x1A" => Some(match (input[7] & (1 << 2) == 0) && (input[7] & (1 << 3) != 0) {
				true => CartridgeType::Nes2,
				false => CartridgeType::Nes,
			}),
			_ => None,
		}
	}
}

pub trait Cartridge {
	fn mapper(&mut self) -> &mut dyn Mapper;
	fn load(memory: &mut Memory, input: Vec<u8>) -> Result<Box<dyn Cartridge>, CartridgeLoadError>
	where
		Self: Sized;
}
